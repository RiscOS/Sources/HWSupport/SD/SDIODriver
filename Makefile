# Makefile for SDIODriver

COMPONENT   = SDIODriver
ASMHDRS     = SDIO SDHCIDevice
ASMCHDRS    = SDIO SDHCIDevice
HDRS        =
CMHGFILE    = SDIOHdr
CMHGDEPENDS = device module op swi
OBJS        = cardreg command device fgopcb globals message module op probe register stuff swi trampoline util
LIBS        = ${ASMUTILS} ${SYNCLIB}
ROMCDEFINES = -DROM_MODULE

# Enable one of the following options, depending on what sort of debug you want

DEBUG ?= FALSE
ifeq (${DEBUG},TRUE)
CFLAGS     += -DDEBUG_ENABLED -DDEBUGLIB -DDEBUGLIB_NOBRACKETS
LIBS       += ${DEBUGLIBS} ${NET5LIBS}
endif
ifeq (${DEBUG},STDOUT)
CFLAGS     += -DDEBUG_ENABLED
endif

include CModule

# This version is used when cross-compiling or with amu >= 5.35

SDHCIDevice.expasmc: SDHCIDevice.hdr SDHCIDevice.h
	${HDR2H} $(word 1, $^) ${C_EXP_HDR:.h=}${SEP}$(word 2, $^)
	${FAPPEND} ${C_EXP_HDR:.h=}${SEP}$(word 2, $^) $(word 2, $^) ${C_EXP_HDR:.h=}${SEP}$(word 2, $^)

# This version is used with amu < 5.35, and can be retired eventually

expasmc.SDHCIDevice: hdr.SDHCIDevice h.SDHCIDevice
	${HDR2H} hdr.SDHCIDevice ${C_EXP_HDR:.h=}.h.SDHCIDevice
	FAppend ${C_EXP_HDR:.h=}.h.SDHCIDevice h.SDHCIDevice ${C_EXP_HDR:.h=}.h.SDHCIDevice

# Dynamic dependencies:
