/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * cddl/RiscOS/Sources/HWSupport/SD/SDIODriver/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2012 Ben Avison.  All rights reserved.
 * Portions Copyright 2019 John Ballance.
 * Use is subject to license terms.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "swis.h"

#include "Global/HALEntries.h"
#include "Global/OSMem.h"
#include "Global/Services.h"
#include "Interface/SDIO.h"

#include "device.h"
#include "globals.h"
#include "register.h"
#include "swi.h"
#include "SDIOHdr.h"
#include "trampoline.h"
#include "util.h"

#define RAM_PAGE_SIZE 4096

static void change_cacheability(bool cacheable, const uint8_t *start, size_t size)
{
  size_t npages = size / RAM_PAGE_SIZE;
  uint32_t opf_flags;
  _swix(OS_PlatformFeatures, _IN(0)|_OUT(0), 0, &opf_flags);
  if (opf_flags & (1u<<21))
  {
    /* RAM pages may have 64-bit physical addresses, so use newer API */
    struct
    {
      /* cppcheck-suppress unusedStructMember */
      uint32_t ppn;
      uint64_t log;
      /* cppcheck-suppress unusedStructMember */
      uint64_t phy;
    } pages[npages];
    for (uint64_t i = 0; i < npages; ++i)
      pages[i].log = (uint64_t) start + i * RAM_PAGE_SIZE;
    _swix(OS_Memory, _INR(0,2), OSMemReason_Convert64 + (1u<<9) + (cacheable ? 3u<<14 : 2u<<14), pages, npages);
  }
  else
  {
    /* RAM pages all in bottom 4 GB of physical address space, so use older API
     * for maximum compatibility */
    struct
    {
      /* cppcheck-suppress unusedStructMember */
      uint32_t ppn;
      uint32_t log;
      /* cppcheck-suppress unusedStructMember */
      uint32_t phy;
    } pages[npages];
    for (uint32_t i = 0; i < npages; ++i)
      pages[i].log = (uint32_t) start + i * RAM_PAGE_SIZE;
    _swix(OS_Memory, _INR(0,2), OSMemReason_Convert + (1u<<9) + (cacheable ? 3u<<14 : 2u<<14), pages, npages);
  }
}

void device_added(sdhcidevice_t *dev)
{
  /* Ignore device if it's not a recognised major version number */
  if ((dev->dev.version & 0xFFFF0000) != HALDeviceSDHCI_MajorVersion_Original &&
      (dev->dev.version & 0xFFFF0000) != HALDeviceSDHCI_MajorVersion_Soft &&
      (dev->dev.version & 0xFFFF0000) != HALDeviceSDHCI_MajorVersion_Soft2)
    return;

  bool is_soft = (dev->dev.version & 0xFFFF0000) == HALDeviceSDHCI_MajorVersion_Soft ||
                 (dev->dev.version & 0xFFFF0000) == HALDeviceSDHCI_MajorVersion_Soft2;
  uint32_t busi;
  size_t op_ws_size = is_soft ? dev->ver.soft.OpWorkspace() : sizeof (sdhci_op_t);
  ctrlnode_t *ctrl = malloc(sizeof *ctrl + dev->slots * (sizeof (sdmmcslot_t) + QUEUE_LENGTH * (sizeof (sdioop_t) + op_ws_size)));
  if (ctrl == NULL)
    return;
  uint8_t *op = (uint8_t *)ctrl + sizeof *ctrl + dev->slots * sizeof (sdmmcslot_t);

  /* Initialise members of the controller struct */
  ctrl->is_soft = is_soft;
  ctrl->devicenumber = -1;
  if (((dev->dev.version & 0xFFFF0000) == HALDeviceSDHCI_MajorVersion_Original &&
       (dev->dev.version & 0x0000FFFF) >= HALDeviceSDHCI_MinorVersion_OriginalValidDevNo) ||
      ((dev->dev.version & 0xFFFF0000) == HALDeviceSDHCI_MajorVersion_Soft &&
       (dev->dev.version & 0x0000FFFF) >= HALDeviceSDHCI_MinorVersion_SoftValidDevNo) ||
       (dev->dev.version & 0xFFFF0000) == HALDeviceSDHCI_MajorVersion_Soft2)
    ctrl->devicenumber = dev->dev.devicenumber;
  if (ctrl->devicenumber != -1)
  {
    ctrl->trampoline = malloc(trampoline_length);
    if (ctrl->trampoline == NULL)
    {
      free(ctrl);
      return;
    }
    memcpy(ctrl->trampoline, (ctrl->devicenumber & HALIRQ_Shared) ? trampoline_vectored : trampoline_unshared, trampoline_length);
    ctrl->trampoline[trampoline_offset_pointer] = (uint32_t) ctrl;
    ctrl->trampoline[trampoline_offset_privateword] = (uint32_t) g_module_pw;
    ctrl->trampoline[trampoline_offset_cmhgveneer] = (uint32_t) module_irq_veneer;
    _swix(OS_SynchroniseCodeAreas, _INR(0,2), 1, ctrl->trampoline, (uint32_t)ctrl->trampoline + trampoline_length - 1);
  }
  ctrl->soft2_flags = 0;
  if ((dev->dev.version & 0xFFFF0000) == HALDeviceSDHCI_MajorVersion_Soft2)
    ctrl->soft2_flags = dev->flags;
  if ((ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_VFP) &&
      _swix(VFPSupport_CreateContext, _INR(0,3)|_OUT(0), 0, 32, 0, 0, &ctrl->vfp_context))
  {
    if (ctrl->devicenumber != -1)
      free(ctrl->trampoline);
    free(ctrl);
    return;
  }

  ctrl->dev = dev;
  ctrl->next = NULL;

  /* Allocate DMA RAM per-slot and notify to the HAL device before Activate() is called */
  for (uint32_t sloti = 0; sloti < dev->slots; sloti++)
  {
    sdmmcslot_t *slot = ctrl->slot + sloti;

    slot->dma_logical = NULL;

    /* If soft version, allocate DMA RAM */
    if (is_soft && dev->ver.soft.GetDMASpace)
    {
      size_t alignment, boundary;
      slot->dma_size = dev->ver.soft.GetDMASpace(dev, sloti, &alignment, &boundary);
      if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_CacheableDMA)
      {
        /* If we're required to manipulate the cacheability, the allocation
         * must be at least page aligned */
        slot->dma_size = (slot->dma_size + (RAM_PAGE_SIZE-1)) &~ (RAM_PAGE_SIZE-1);
        alignment = (alignment + (RAM_PAGE_SIZE-1)) &~ (RAM_PAGE_SIZE-1);
      }
      if (slot->dma_size != 0)
      {
        uint32_t dma_physical;
        if (_swix(PCI_RAMAlloc, _INR(0,2)|_OUTR(0,1), slot->dma_size, alignment, boundary, &slot->dma_logical, &dma_physical) != NULL)
        {
          /* Failed to allocate DMA RAM */
          for (uint32_t sloti2 = 0; sloti2 < sloti; sloti2++)
          {
            sdmmcslot_t *slot2 = ctrl->slot + sloti2;
            if (slot2->dma_logical != NULL)
            {
              if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_CacheableDMA)
                change_cacheability(false, slot->dma_logical, slot->dma_size);
              _swix(PCI_RAMFree, _IN(0), slot2->dma_logical);
            }
          }
          if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_VFP)
            _swix(VFPSupport_DestroyContext, _IN(0), ctrl->vfp_context);
          if (ctrl->devicenumber != -1)
            free(ctrl->trampoline);
          free(ctrl);
          return;
        }
        if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_CacheableDMA)
          change_cacheability(true, slot->dma_logical, slot->dma_size);
        dev->ver.soft.SetDMASpace(dev, sloti, slot->dma_logical, dma_physical);
      }
    }
  }

  /* Initialise the controller hardware */
  if (dev->dev.Activate(&dev->dev) == false)
  {
    /* Controller initialisation failed */
    for (uint32_t sloti = 0; sloti < dev->slots; sloti++)
    {
      sdmmcslot_t *slot = ctrl->slot + sloti;
      if (slot->dma_logical != NULL)
      {
        if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_CacheableDMA)
          change_cacheability(false, slot->dma_logical, slot->dma_size);
        _swix(PCI_RAMFree, _IN(0), slot->dma_logical);
      }
    }
    if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_VFP)
      _swix(VFPSupport_DestroyContext, _IN(0), ctrl->vfp_context);
    if (ctrl->devicenumber != -1)
      free(ctrl->trampoline);
    free(ctrl);
    return;
  }

  /* Initialise members of each slot struct */
  for (uint32_t sloti = 0; sloti < dev->slots; sloti++)
  {
    sdhci_regset_t      *regs = dev->slotinfo[sloti].stdregs;
    sdhci_writebuffer_t *wrbuf = &ctrl->slot[sloti].wrbuf;
    bool                 force32 = dev->flags & HALDeviceSDHCI_Flag_32bit;
    sdmmcslot_t         *slot = ctrl->slot + sloti;

    slot->insertion_pending = false;
    slot->write_protect = true;
    slot->no_card_detect = dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_NoCardDetect;
    slot->busy = MUTEX_UNLOCKED;
    slot->doing_poll = (const spinrwlock_t) SPINRW_INITIALISER;
    slot->doing_card_detect = MUTEX_UNLOCKED;
    _swix(OS_ReadMonotonicTime, _OUT(0), &slot->card_detect_debounce_time);
    slot->card_detect_state = CD_DEBOUNCING; /* as though we're at the end of debounce already */
    slot->memory_type = MEMORY_NONE;
    slot->io_functions = 0;
    register_init_buffer(wrbuf);
    slot->op_used = 0;
    slot->op_free = 0;
    for (size_t i = 0; i < QUEUE_LENGTH; i++)
    {
        slot->op[i] = (sdioop_t *) op;
        op += sizeof (sdioop_t) + op_ws_size;
    }
    slot->op_lock = (const spinlock_t) SPIN_INITIALISER;
    slot->selected_rca = 0;
    slot->sdio_int_lock = (const spinlock_t) SPIN_INITIALISER;
    slot->sdio_int_state = SDIO_STATE_IDLE;
    slot->sdio_int_op = NULL;
    slot->sdio_int_function = 0;
    slot->sdio_int_handler_count = 0;
    memset(&slot->sdio_int_handler, 0, sizeof slot->sdio_int_handler);
    memset(&slot->sdio_int_handler_pw, 0, sizeof slot->sdio_int_handler_pw);
#if DEBUG_SDIO
    slot->fake_sdio_int = false;
    slot->fake_sdio_int_enable = false;
#endif

    if (!is_soft)
    {
      /* Mask all interrupts for this slot */
      slot->normal_irqs_lock = (const spinlock_t) SPIN_INITIALISER;
      slot->normal_irqs = 0;
      REGISTER_WRITE(normal_interrupt_signal_enable, 0);
      REGISTER_WRITE(error_interrupt_signal_enable, 0);
      REGISTER_WRITE(normal_interrupt_status_enable, 0);
      REGISTER_WRITE(error_interrupt_status_enable, 0);
    }

    /* Turn off activity LEDs */
    set_activity(ctrl, sloti, HALDeviceSDHCI_ActivityOff);

    /* Read and interpret capabilities for this slot */
    slot->spec_rev = is_soft ? 0 : REGISTER_READ(host_controller_version) & HCV_SREV_MASK;
    if (dev->GetCapabilities)
    {
      uint64_t capabilities = dev->GetCapabilities(dev, sloti);
      slot->caps[0] = (uint32_t) capabilities;
      slot->caps[1] = (uint32_t) (capabilities >> 32);
    }
    else
    {
      slot->caps[0] = REGISTER_READ(capabilities[0]);
      slot->caps[1] = slot->spec_rev < HCV_SREV_3_00 ? 0 : REGISTER_READ(capabilities[1]);
    }
    if (dev->GetVddCapabilities)
      slot->voltage_caps = dev->GetVddCapabilities(dev, sloti) * CAP0_VS33;
    else
      slot->voltage_caps = slot->caps[0] & (CAP0_VS33 | CAP0_VS30 | CAP0_VS18);
    slot->controller_ocr = 0;
    if (slot->voltage_caps & CAP0_VS18)
      slot->controller_ocr |= OCR_1_7V_1_95V;
    if (slot->voltage_caps & CAP0_VS30)
      slot->controller_ocr |= OCR_2_9V_3_0V | OCR_3_0V_3_1V;
    if (slot->voltage_caps & CAP0_VS33)
      slot->controller_ocr |= OCR_3_2V_3_3V | OCR_3_3V_3_4V;
    switch (slot->caps[0] & CAP0_MBL_MASK)
    {
      case CAP0_MBL_2048: slot->controller_maxblklen = 11; break;
      case CAP0_MBL_1024: slot->controller_maxblklen = 10; break;
      default:            slot->controller_maxblklen =  9; break;
    }
    if (dev->flags & HALDeviceSDHCI_Flag_BlockCountLimitMask)
      slot->controller_maxblkcount = 1 << (((dev->flags & HALDeviceSDHCI_Flag_BlockCountLimitMask) >> HALDeviceSDHCI_Flag_BlockCountLimitShift) - 1);
    else
      slot->controller_maxblkcount = 0xFFFF;
  }

  /* Find a unique bus number and insert controller block into global list,
   * now that it is fully initialised */
  spinrw_write_lock(&g_ctrl_lock);
  for (busi = 0;; busi++)
  {
    ctrlnode_t *prev, *this;
    for (prev = NULL, this = g_ctrl; this != NULL && this->bus != busi; prev = this, this = this->next);
    if (this == NULL)
    {
      if (prev == NULL)
        g_ctrl = ctrl;
      else
        prev->next = ctrl;
      ctrl->bus = busi;
      break;
    }
  }

  /* In order to keep using ctrl now that it's in the global list, we need a
   * read lock on it. */
  spinrw_write_to_read(&g_ctrl_lock);

  if (ctrl->devicenumber != -1)
  {
    /* Install interrupt handler for this controller - needed before we can issue commands */
    _swix(OS_ClaimDeviceVector, _INR(0,2), ctrl->devicenumber, ctrl->trampoline, dev);
    /* Enable the interrupt in the interrupt controller */
    _swix(OS_Hardware, _IN(0)|_INR(8,9), ctrl->devicenumber &~ HALIRQ_Shared, 0, EntryNo_HAL_IRQEnable);
  }

  /* Issue the service calls so our clients know we're here.
   * Note that we must be ready to handle any SWIs for this bus by this point. */
  for (uint32_t sloti = 0; sloti < dev->slots; sloti++)
    _swix(OS_ServiceCall, _INR(1,2), Service_SDIOSlotAttached,
        (sloti << SDIOSlotAttached_SlotShift) |
        (ctrl->bus << SDIOSlotAttached_BusShift) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_Integrated) ? SDIOSlotAttached_Integrated : 0) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_NoCardDetect) ? SDIOSlotAttached_NoCardDetect : 0) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_IntegratedMem) ? SDIOSlotAttached_IntegratedMem : 0) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_BrokenCMD23) ? SDIOSlotAttached_BrokenCMD23 : 0));

  /* Reset and probe the bus */
  if (swi_Initialise_reset_bus(busi) != NULL)
  {
    /* Failed, so clean up */
    spinrw_read_unlock(&g_ctrl_lock);
    device_removed(dev);
    return;
  }

  /* We've finished with the ctrl and slot pointers now */
  spinrw_read_unlock(&g_ctrl_lock);
}

void device_removed(sdhcidevice_t *dev)
{
  /* Ignore device if it's not a recognised major version number */
  if ((dev->dev.version & 0xFFFF0000) != HALDeviceSDHCI_MajorVersion_Original &&
      (dev->dev.version & 0xFFFF0000) != HALDeviceSDHCI_MajorVersion_Soft &&
      (dev->dev.version & 0xFFFF0000) != HALDeviceSDHCI_MajorVersion_Soft2)
    return;

  ctrlnode_t *prev, *ctrl;

  /* First thing to do is issue the service calls, while the bus index is still
   * guaranteed to be unique. Do this while holding a read lock - it we make a
   * service call with a write lock held, we'd get deadlocks if anyone tries to
   * call any of our SWIs from the service call handler. */
  spinrw_read_lock(&g_ctrl_lock);
  for (prev = NULL, ctrl = g_ctrl; ctrl != NULL && ctrl->dev != dev; prev = ctrl, ctrl = ctrl->next);
  if (ctrl == NULL) /* shouldn't happen, but just in case */
  {
    spinrw_read_unlock(&g_ctrl_lock);
    return;
  }
  for (uint32_t sloti = 0; sloti < dev->slots; sloti++)
  {
    sdmmcslot_t *slot = ctrl->slot + sloti;
    mutex_lock(&slot->doing_card_detect); /* might as well leave it claimed now */
    if (slot->card_detect_state == CD_ANNOUNCED)
      device_units_detached(ctrl, sloti);
    _swix(OS_ServiceCall, _INR(1,2), Service_SDIOSlotDetached,
        (sloti << SDIOSlotAttached_SlotShift) |
        (ctrl->bus << SDIOSlotAttached_BusShift) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_Integrated) ? SDIOSlotAttached_Integrated : 0) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_NoCardDetect) ? SDIOSlotAttached_NoCardDetect : 0) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_IntegratedMem) ? SDIOSlotAttached_IntegratedMem : 0) |
        ((dev->slotinfo[sloti].flags & HALDeviceSDHCI_SlotFlag_BrokenCMD23) ? SDIOSlotAttached_BrokenCMD23 : 0));
  }
  spinrw_read_unlock(&g_ctrl_lock);

  /* Find controller struct from HAL device pointer, and delink it from the list
   * of controllers. This time we need a write lock. */
  spinrw_write_lock(&g_ctrl_lock);
  for (prev = NULL, ctrl = g_ctrl; ctrl != NULL && ctrl->dev != dev; prev = ctrl, ctrl = ctrl->next);
  if (ctrl == NULL) /* shouldn't happen, but just in case */
  {
    spinrw_write_unlock(&g_ctrl_lock);
    return;
  }
  if (prev == NULL)
    g_ctrl = ctrl->next;
  else
    prev->next = ctrl->next;
  spinrw_write_unlock(&g_ctrl_lock);

  if (!ctrl->is_soft)
  {
    /* Mask interrupts in the SD controller */
    for (uint32_t sloti = 0; sloti < dev->slots; sloti++)
    {
      sdhci_regset_t      *regs = dev->slotinfo[sloti].stdregs;
      sdhci_writebuffer_t *wrbuf = &ctrl->slot[sloti].wrbuf;
      bool                 force32 = dev->flags & HALDeviceSDHCI_Flag_32bit;

      REGISTER_WRITE(normal_interrupt_signal_enable, 0);
      REGISTER_WRITE(error_interrupt_signal_enable, 0);
      REGISTER_WRITE(normal_interrupt_status_enable, 0);
      REGISTER_WRITE(error_interrupt_status_enable, 0);
    }
  }

  /* Tell the HAL device to do its own cleanup */
  dev->dev.Deactivate(&dev->dev);

  /* Free any allocated DMA RAM */
  for (uint32_t sloti = 0; sloti < dev->slots; sloti++)
  {
    sdmmcslot_t *slot = ctrl->slot + sloti;
    if (slot->dma_logical != NULL)
    {
      if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_CacheableDMA)
        change_cacheability(false, slot->dma_logical, slot->dma_size);
      _swix(PCI_RAMFree, _IN(0), slot->dma_logical);
    }
  }

  /* Any VFP context now needs to be freed */
  if (ctrl->soft2_flags & HALDeviceSDHCI_Flag_Soft2_VFP)
    _swix(VFPSupport_DestroyContext, _IN(0), ctrl->vfp_context);

  if (ctrl->devicenumber != -1)
  {
    /* Leave the interrupt enabled in the interrupt controller as per standard RISC OS practice */

    /* Deregister interrupt handler */
    _swix(OS_ReleaseDeviceVector, _INR(0,2), ctrl->devicenumber, ctrl->trampoline, dev);

    /* Release memory */
    free(ctrl->trampoline);
  }
  free(ctrl);
}

static void issue_unit_service_calls(ctrlnode_t *ctrl, uint32_t sloti, uint32_t service)
{
  sdmmcslot_t *slot = ctrl->slot + sloti;
  for (uint32_t cardi = 0; cardi < MAX_CARDS_PER_SLOT; cardi++)
  {
    sdmmccard_t *card = slot->card + cardi;
    if (card->in_use)
    {
      uint32_t r2 = (sloti << SDIOUnitAttached_SlotShift) |
          (ctrl->bus << SDIOUnitAttached_BusShift) |
          (card->rca << SDIOUnitAttached_RCAShift);
      if (slot->memory_type != MEMORY_NONE)
      {
        uint32_t r3 = 0 << SDIOUnitAttached_UnitShift;
        if (slot->write_protect)
          r3 |= SDIOUnitAttached_WriteProtect;
        if (slot->memory_type == MEMORY_MMC)
          r3 |= SDIOUnitAttached_MMC;
        _swix(OS_ServiceCall, _INR(1,5), service, r2, r3, (uint32_t) card->capacity, (uint32_t) (card->capacity >> 32));
      }
      for (uint32_t function = 1; function <= slot->io_functions; function++)
      {
        uint32_t r3 = function << SDIOUnitAttached_UnitShift;
        r3 |= slot->fic[function-1] << SDIOUnitAttached_FICShift;
        if (slot->write_protect)
          r3 |= SDIOUnitAttached_WriteProtect;
        _swix(OS_ServiceCall, _INR(1,5), service, r2, r3, slot->io_manf[function-1], slot->io_card[function-1]);
      }
    }
  }
}

void device_units_attached(ctrlnode_t *ctrl, uint32_t sloti)
{
  sdmmcslot_t *slot = ctrl->slot + sloti;
  /* Assume the doing_card_detect mutex is already held on entry */
  if (slot->card_detect_state == CD_OCCUPIED)
  {
    issue_unit_service_calls(ctrl, sloti, Service_SDIOUnitAttached);
    slot->card_detect_state = CD_ANNOUNCED;
  }
  dprintf("Card inserted (write-protect = %d)\n", slot->write_protect);
}

void device_units_detached(ctrlnode_t *ctrl, uint32_t sloti)
{
  sdmmcslot_t *slot = ctrl->slot + sloti;
  /* Assume the doing_card_detect mutex is already held on entry */
  if (slot->card_detect_state == CD_ANNOUNCED)
  {
    issue_unit_service_calls(ctrl, sloti, Service_SDIOUnitDetached);
    slot->card_detect_state = CD_OCCUPIED;
  }
  dprintf("Card removed  ");
}
